/*
 * rssreader.c: A plugin for the Video Disk Recorder
 *
 */

#include <getopt.h>
#include <vdr/config.h>
#include <vdr/plugin.h>
#include <vdr/menu.h>

#include "menu.h"
#include "config.h"
#include "common.h"

#if defined(APIVERSNUM) && APIVERSNUM < 20000
#error "VDR-2.0.0 API version or greater is required!"
#endif

#ifndef GITVERSION
#define GITVERSION ""
#endif

static const char VERSION[]       = "2.0.1" GITVERSION;
static const char DESCRIPTION[]   = trNOOP("RSS Reader for OSD");
static const char MAINMENUENTRY[] = trNOOP("RSS Reader");

class cPluginRssReader : public cPlugin {
private:
  // Add any member variables or functions you may need here.
public:
  cPluginRssReader(void);
  virtual ~cPluginRssReader();
  virtual const char *Version(void) { return VERSION; }
  virtual const char *Description(void) { return tr(DESCRIPTION); }
  virtual const char *CommandLineHelp(void);
  virtual bool ProcessArgs(int argc, char *argv[]);
  virtual bool Initialize(void);
  virtual bool Start(void);
  virtual void Stop(void);
  virtual void Housekeeping(void);
  virtual void MainThreadHook(void) {}
  virtual cString Active(void) { return NULL; }
  virtual const char *MainMenuEntry(void) { return (RssConfig.hidemenu ? NULL : tr(MAINMENUENTRY)); }
  virtual cOsdObject *MainMenuAction(void);
  virtual cMenuSetupPage *SetupMenu(void);
  virtual bool SetupParse(const char *Name, const char *Value);
  virtual bool Service(const char *Id, void *Data = NULL);
  virtual const char **SVDRPHelpPages(void);
  virtual cString SVDRPCommand(const char *Command, const char *Option, int &ReplyCode);
  };

class cPluginRssReaderSetup : public cMenuSetupPage
{
private:
  cRssReaderConfig data;
  cVector<const char*> help;
  void Setup(void);
protected:
  virtual eOSState ProcessKey(eKeys Key);
  virtual void Store(void);
public:
  cPluginRssReaderSetup(void);
};

cPluginRssReader::cPluginRssReader(void)
{
  // Initialize any member variables here.
  // DON'T DO ANYTHING ELSE THAT MAY HAVE SIDE EFFECTS, REQUIRE GLOBAL
  // VDR OBJECTS TO EXIST OR PRODUCE ANY OUTPUT!
}

cPluginRssReader::~cPluginRssReader()
{
  // Clean up after yourself!
}

const char *cPluginRssReader::CommandLineHelp(void)
{
  // Return a string that describes all known command line options.
  return NULL;
}

bool cPluginRssReader::ProcessArgs(int argc, char *argv[])
{
  // Implement command line argument processing here if applicable.
  return true;
}

bool cPluginRssReader::Initialize(void)
{
  // Initialize any background activities the plugin shall perform.
  strn0cpy(RssConfig.configfile, AddDirectory(ConfigDirectory(PLUGIN_NAME_I18N), RSSREADER_CONF), sizeof(RssConfig.configfile));
  return true;
}

bool cPluginRssReader::Start(void)
{
  // Start any background activities the plugin shall perform.
  if (!RssItems.Load(RssConfig.configfile))
     error("configuration file '" RSSREADER_CONF "' not found!");
  return true;
}

void cPluginRssReader::Stop(void)
{
  // Stop any background activities the plugin shall perform.
}

void cPluginRssReader::Housekeeping(void)
{
  // Perform any cleanup or other regular tasks.
}

cOsdObject *cPluginRssReader::MainMenuAction(void)
{
  // Perform the action when selected from the main VDR menu.
  return new cRssStreamsMenu();
}

cMenuSetupPage *cPluginRssReader::SetupMenu(void)
{
  // Return a setup menu in case the plugin supports one.
  return new cPluginRssReaderSetup();
}

bool cPluginRssReader::SetupParse(const char *Name, const char *Value)
{
  // Parse your own setup parameters and store their values.
  if      (!strcasecmp(Name, "HideMenu"))  RssConfig.hidemenu = atoi(Value);
  else if (!strcasecmp(Name, "HideElem"))  RssConfig.hideelem = atoi(Value);
  else if (!strcasecmp(Name, "UseProxy"))  RssConfig.useproxy = atoi(Value);
  else if (!strcasecmp(Name, "HttpProxy")) strn0cpy(RssConfig.httpproxy, Value, sizeof(RssConfig.httpproxy));
  else return false;

  return true;
}

bool cPluginRssReader::Service(const char *Id, void *Data)
{
  // Handle custom service requests from other plugins
  return false;
}

const char **cPluginRssReader::SVDRPHelpPages(void)
{
  static const char *HelpPages[] = {
    "LOAD\n"
    "    Load RSS feed configuration file.",
    NULL
    };
  return HelpPages;
}

cString cPluginRssReader::SVDRPCommand(const char *Command, const char *Option, int &ReplyCode)
{
  if (strcasecmp(Command, "LOAD") == 0) {
     if (!RssItems.Load(RssConfig.configfile)) {
        ReplyCode = 550; // Requested action not taken
        return cString("Configuration file not found!");
        }
     return cString("Configuration file loaded");
     }
  return NULL;
}

cPluginRssReaderSetup::cPluginRssReaderSetup(void)
: data(RssConfig)
{
  SetMenuCategory(mcSetupPlugins);
  Setup();
  SetHelp(tr("Button$Load"), NULL, NULL, NULL);
}

void cPluginRssReaderSetup::Setup(void)
{
  int current = Current();

  Clear();
  help.Clear();

  Add(new cMenuEditBoolItem(tr("Hide main menu entry"), &data.hidemenu));
  help.Append(tr("Define whether the main manu entry is hidden."));

  Add(new cMenuEditBoolItem(tr("Hide non-existent elements"), &data.hideelem));
  help.Append(tr("Define whether all non-existent RSS stream elements are hidden."));

  Add(new cMenuEditBoolItem(tr("Use HTTP proxy server"), &data.useproxy));
  help.Append(tr("Define whether a HTTP proxy server is used."));

  if (data.useproxy) {
     Add(new cMenuEditStrItem( tr("HTTP proxy server"), data.httpproxy, sizeof(data.httpproxy), tr(FileNameChars)));
     help.Append(tr("Define an address and port of the HTTP proxy server:\n\n\"proxy.domain.com:8000\""));
     }

  SetCurrent(Get(current));
  Display();
}

eOSState cPluginRssReaderSetup::ProcessKey(eKeys Key)
{
  int olduseproxy = data.useproxy;
  eOSState state = cMenuSetupPage::ProcessKey(Key);

  if (Key != kNone && (data.useproxy != olduseproxy))
     Setup();

  if (state == osUnknown) {
     switch (Key) {
       case kRed:
            Skins.Message(mtInfo, tr("Loading configuration file..."));
            RssItems.Load(RssConfig.configfile);
            Skins.Message(mtInfo, NULL);
            state = osContinue;
            break;
       case kInfo:
            if (Current() < help.Size())
               return AddSubMenu(new cMenuText(cString::sprintf("%s - %s '%s'", tr("Help"), trVDR("Plugin"), PLUGIN_NAME_I18N), help[Current()]));
            break;
       default:
            break;
       }
     }

  return state;
}

void cPluginRssReaderSetup::Store(void)
{
  RssConfig = data;
  SetupStore("HideMenu",  RssConfig.hidemenu);
  SetupStore("HideElem",  RssConfig.hideelem);
  SetupStore("UseProxy",  RssConfig.useproxy);
  SetupStore("HttpProxy", RssConfig.httpproxy);
}

VDRPLUGINCREATOR(cPluginRssReader); // Don't touch this!
